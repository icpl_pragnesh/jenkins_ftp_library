// See https://github.com/jenkinsci/workflow-cps-global-lib-plugin
/// Example code to call ftp function
//ftp(['host':"",
//'credential_id':"",
//'local_directory':"",
//'remote_diectory':"",
//'after_upload_command':""])


import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

def call(config) {

try {

withCredentials([usernamePassword(credentialsId: config.credential_id, passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
	env.USERNAME = USERNAME
	env.PASSWORD = PASSWORD
}

FTPClient ftpClient = new FTPClient();

ftpClient.connect(config.host, 21)
if (ftpClient.login(USERNAME, PASSWORD)) {

ftpClient.enterLocalPassiveMode();

remote_dir_path = config.remote_diectory

// Create Directories from remote path if it does not exist.
directories = remote_dir_path.split('/');
dir_path = ""
for (String directory : directories) {
	dir_path = dir_path + "/" + directory
	createRemoteDirectory(ftpClient, dir_path)
}

File localFiles = new File(config.local_directory);
if (localFiles.isFile()) {
	uploadSingleFile(ftpClient, localFiles.getAbsolutePath(), remote_dir_path+"/"+localFiles.getName());
} else {
	if (!config.local_directory.endsWith("/")) {
		remote_dir_path = remote_dir_path + "/" + config.local_directory.substring(config.local_directory.lastIndexOf('/') + 1)
		createRemoteDirectory(ftpClient, remote_dir_path)
	}
uploadDirectory(ftpClient, remote_dir_path, config.local_directory, "");
}

if(config.after_upload_command != null) {
	ftpClient.doCommand(config.after_upload_command, "")
}
} else {
	throw new Exception("FTP Login Failed")
}

ftpClient.logout();
ftpClient.disconnect();

} catch (error) {
	print error
	throw error
}
}






def uploadDirectory(ftpClient, remoteDirPath, localParentDir, remoteParentDir) {

File localFiles = new File(localParentDir);

File[] subFiles = localFiles.listFiles();
if (subFiles != null && subFiles.length > 0) {
for (File item : subFiles) {
String remoteFilePath = remoteDirPath + "/" + remoteParentDir + "/" + item.getName();
if (remoteParentDir.equals("")) {
remoteFilePath = remoteDirPath + "/" + item.getName();
}

if (item.isFile()) {
uploadSingleFile(ftpClient, item.getAbsolutePath(), remoteFilePath);
} else {
createRemoteDirectory(ftpClient, remoteFilePath)

// upload the sub directory
String parent = remoteParentDir + "/" + item.getName();
if (remoteParentDir.equals("")) {
parent = item.getName();
}

localParentDir = item.getAbsolutePath();
uploadDirectory(ftpClient, remoteDirPath, localParentDir, parent);
}
}
}
}



def createRemoteDirectory(ftpClient, remoteDirectoryPath) {
boolean created = ftpClient.makeDirectory(remoteDirectoryPath);
if (created) { print("CREATED the directory: " + remoteDirectoryPath);
} else { print("COULD NOT create the directory: " + remoteDirectoryPath); }
}



def uploadSingleFile(ftpClient, localFilePath, remoteFilePath) {
File localFile = new File(localFilePath);

InputStream inputStream = new FileInputStream(localFile);
try {
ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
boolean uploaded = ftpClient.storeFile(remoteFilePath, inputStream);

if (uploaded) { print("UPLOADED a file to: " + remoteFilePath);
} else { print("COULD NOT upload the file: " + localFilePath); }

return uploaded;
} finally {
inputStream.close();
}
}
