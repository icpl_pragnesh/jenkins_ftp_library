// See https://github.com/jenkinsci/workflow-cps-global-lib-plugin
/// Example code to call ftp function
//ios_ipa_upload(['host':"",
//'credential_id':"",
//'local_directory':"",
//'remote_diectory':"",
//'after_upload_command':"",
//'create_html_download':false,
//'ipa_name':"",
//'ipa_path':"",
//'icon_path':"",
//'itunesArtworkPath':"",
//'bundle_identifier':"",
//'app_name':"",
//'url':""])


def call(config) {

result_path = ""

sh ''' set +x ; date +\"%Y%m%d%H%M%S\" > timestamp '''
ipa_name = readFile('timestamp').trim()+"_"+config.ipa_name;

dir ("temp_ftp_upload") {
temp_ftp_folder = pwd();

Files.copy(config.ipa_path, temp_ftp_folder+'/'+ipa_name)
Files.copy(config.icon_path, temp_ftp_folder+'/Icon.png')
Files.copy(config.itunesArtworkPath, temp_ftp_folder+'/iTunesArtwork.png')

ftp_path = config.url+"/"+config.remote_diectory
plist_name = config.app_name+'.plist'

def plistFile = libraryResource 'org/is/ftp/template.plist'
plistFile.replace("${APP_NAME}", config.app_name)
plistFile.replace("${BUNDLE_IDENTIFIER}", config.bundle_identifier)
plistFile.replace("${IPA_NAME}", ipa_name)
plistFile.replace("${FTP_APP_URL}", ftp_path)
writeFile file: temp_ftp_folder+'/'+plist_name, text: plistFile
result_path = ftp_path+'/'+plist_name

if (config.create_html_download) {
	def htmlFile = libraryResource 'org/is/ftp/index.html'
	htmlFile.replace("${FTP_PLIST}", result_path)
	writeFile file: temp_ftp_folder+'/index.html', text: htmlFile
	result_path = ftp_path+'/index.html'
}

config.local_directory = temp_ftp_folder+"/"
}


ftp(config)

dir ("temp_ftp_upload") {
	deleteDir()
}

retutn result_path
}
